# Vite Bootstrap Boilerplate

![Laubach Eifel](https://img.shields.io/badge/Built%20in-Laubach%20Eifel-critical.svg?logo=vite)
![node](https://img.shields.io/badge/node-%3E%3D%2016.0.0-brightgreen)
[![MIT License](https://img.shields.io/github/license/leandroDCI/webpack5-boilerplate.svg)](LICENSE)

- Transpiling to ECMAScript 2015
- CSS extraction into a single file using
- SCSS support
- [Bootstrap5](https://getbootstrap.com/)
- [ESlint](https://eslint.org/) linting
- [Cypress](https://docs.cypress.io/guides/overview/why-cypress) integration
- uses [@fontsoure](https://fontsource.org/)
- uses [aos](https://michalsnik.github.io/aos/)
- [fontawesome with fortawesome](https://fortawesome.com/)
- [unplugin-icons web-component](https://github.com/unplugin/unplugin-icons/blob/main/examples/vite-web-components/index.html)

**tbc:** [Lighthouse check](https://www.npmjs.com/package/lighthouse)

- [Demo Gitlab Pages](https://avhulst.gitlab.io/vite-4-bootstrap-boilerplate/)

### Commands

#### Development

Install Node Packages

```bash
yarn
```

...start coding!

```bash
yarn dev
```

#### Production

```bash
yarn build
```

#### Testing

```bash
yarn cypress
```

#### Eslint

```bash
yarn lint
```

## Setup

### Quick setup

Create a directory for your new project, clone this repository, install the required modules and start coding!

```bash
npx tiged https://gitlab.com/avhulst/vite-4-bootstrap-boilerplate.git MyProject && cd MyProject
```

```bash
yarn && yarn dev
```

