// https://applitools.com/blog/using-cypress-google-lighthouse-performance-testing/
describe('Lighthouse', () => {
    it('should run performance audits', () => {
        cy.visit('http://localhost:3000');
        cy.lighthouse();
    });
});
