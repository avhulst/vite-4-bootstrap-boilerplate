import * as bootstrap from 'bootstrap';
//import { Tooltip, Toast, Popover } from "bootstrap";

import '../scss/main.scss';

import './fontawesome.js';
import './iconify';

import '@fontsource/catamaran/latin-ext-800.css';
import '@fontsource/lato/latin-ext-400.css';

import '@fontsource-variable/catamaran';
import '@fontsource-variable/noto-sans';

import AOS from 'aos';
import 'aos/dist/aos.css';

AOS.init();
