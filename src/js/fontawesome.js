import {library, icon, dom} from '@fortawesome/fontawesome-svg-core';

import {
    faCamera,
    faCircleXmark,
    faCookieBite,
    faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import {
    faJs,
    faHtml5,
    faSass,
    faGitlab,
    faGithub,
    faYarn,
    faNodeJs,
} from '@fortawesome/free-brands-svg-icons';

library.add(
    faCamera,
    faChevronRight,
    faCookieBite,
    faCircleXmark,
    faJs,
    faHtml5,
    faSass,
    faGitlab,
    faGithub,
    faYarn,
    faNodeJs
);

dom.watch();
