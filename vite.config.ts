import { resolve } from 'path';
import type { UserConfig } from 'vite'
import Icons from 'unplugin-icons/vite'

const path = require('path');

const config: UserConfig = {
    plugins: [
        Icons({
            compiler: 'web-components',
            webComponents: {
                autoDefine: true,
            },
        }),
    ],
    root: path.resolve(__dirname, 'src'),
    publicDir: path.resolve(__dirname, 'static_assets'),
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, 'src/index.html'),
                impress: resolve(__dirname, 'src/impressum.html'),
                privacy: resolve(__dirname, 'src/datenschutz.html'),
            },
        },
        outDir: path.join(__dirname, 'public'),
        target: ['es2015', 'chrome58', 'firefox57', 'safari11'],
    },
    resolve: {
        alias: {
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
        },
    },
    server: {
        port: 8080,
        //hot: true,
    },
    preview: {
        port: 8081,
    },
}
/*
export default {

};
*/

export default config
